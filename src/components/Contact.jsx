import React, {Component} from 'react';
import { NavLink } from "react-router-dom";

class Contact extends Component {
  render() {
    return (
      <NavLink to={`${this.props.chatUrl}`} style={{ color: '#fff' }}>
        <li className="contact">
          <div className="wrap">
            <span className={`contact-status ${this.props.status}`}/>
            <img src="http://emilcarlsson.se/assets/louislitt.png" alt=""/>
            <div className="meta">
              <p className="name">{this.props.name}</p>
              {/*<p className="preview">You just got LITT up, Mike.</p>*/}
            </div>
          </div>
        </li>
      </NavLink>
    );
  }
}

export default Contact;