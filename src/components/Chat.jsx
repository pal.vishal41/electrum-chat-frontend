import React, {Component} from 'react';
import Sidepanel from "./Sidepanel";
import WebSocketInstance from '../websocket';
import {connect} from "react-redux";

class Chat extends Component {

  state = {
    message: '',
  };

  initializeChat = () => {
    this.waitForSocketConnection(() => {
      WebSocketInstance.addCallbacks(
        this.setMessages.bind(this),
        this.addMessage.bind(this)
      );
      WebSocketInstance.fetchMessages(this.props.auth.username, this.props.match.params.chatId);
    });
    WebSocketInstance.connect(this.props.match.params.chatId);
  };

  constructor(props) {
    super(props);
    this.state = {};
    this.initializeChat();
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.match.params.chatId !== nextProps.match.params.chatId) {
      WebSocketInstance.disconnect();
      this.waitForSocketConnection(() => {
        WebSocketInstance.fetchMessages(nextProps.auth.username, nextProps.match.params.chatId);
      });
      WebSocketInstance.connect(this.props.match.params.chatId);
    }
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  waitForSocketConnection(callback) {
    const component = this;
    setTimeout(
      function() {
        if(WebSocketInstance.state() === 1){
          console.log('Connection is secure');
          callback();
        } else {
          console.log('waiting for connection');
          component.waitForSocketConnection(callback);
        }
      }, 100
    )
  }

  addMessage(message) {
    this.setState({ messages: [...this.state.messages, message] });
  }

  onMessageChange = e => {
    this.setState({ message: e.target.value });
  };

  sendMessage = e => {
    e.preventDefault();
    const message = {
      from: this.props.auth.username,
      content: this.state.message,
      chat_id: this.props.match.params.chatId,
    };
    WebSocketInstance.newChatMessage(message);
    this.setState({message: ''});
  };

  renderTimeStamp = timestamp => {
    let prefix = '';
    const timeDiff = Math.round((new Date().getTime() - new Date(timestamp).getTime())/60000);
    if(timeDiff < 60) prefix = `${timeDiff} minutes ago`;
    else if(timeDiff > 60 && timeDiff < 24*60) prefix = `${Math.round(timeDiff/60)} hours ago`;
    else if(timeDiff > 24*60 && timeDiff < 31*24*60) prefix = `${Math.round(timeDiff/(60*24))} days ago`;
    else prefix = `${ new Date(timestamp) }`;
    return prefix;
  };

  renderMessages = () => {
    const currentUser = this.props.auth.username;
    return this.state.messages.map(message => {
      return (
        <li key={message.id} className={message.author === currentUser ? 'sent' : 'replies'}>
          <img alt={""} src={"http://emilcarlsson.se/assets/mikeross.png"} />
          <p>
            {message.content}
            <br/>
            <small style={{ fontSize: '10px' }}>
              { this.renderTimeStamp(message.timestamp) }
            </small>
          </p>
        </li>
      );
    })
  };

  setMessages(messages) {
    if(messages) this.setState({ messages: messages.reverse() });
  }

  render() {
    return (
      <div id="frame">
        <Sidepanel/>
        <div className="content">
          <div className="contact-profile">
            <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt=""/>
            <p>{this.props.auth.username}</p>
            <div className="social-media">
              <i className="fa fa-facebook" aria-hidden="true"></i>
              <i className="fa fa-twitter" aria-hidden="true"></i>
              <i className="fa fa-instagram" aria-hidden="true"></i>
            </div>
          </div>
          <div className="messages">
            <ul id="chat-log">
              { this.state.messages ? this.renderMessages() : null}
              <div style={{ float: "left", clear: "both" }}
                ref={el=>this.messagesEnd = el}
              >

              </div>
            </ul>
          </div>
          <div className="message-input">
            <div className="wrap">
              <form onSubmit={this.sendMessage}>
                <input onChange={this.onMessageChange} value={this.state.message} id="chat-message-input" type="text" placeholder="Write your message..."/>
                <i className="fa fa-paperclip attachment" aria-hidden="true"/>
                <button id="chat-message-submit" type={"submit"} className="submit">
                  <i className="fa fa-paper-plane" aria-hidden="true"/>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return { auth };
};

export default connect(mapStateToProps)(Chat);