import React, {Component} from 'react';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';
import { connect } from "react-redux";
import { authLogin, authSignup, logout } from "../actions/auth";
import Contact from "./Contact";
import axios from 'axios';

class Sidepanel extends Component {
  state = {
    login: true,
    username: '',
    password: '',
    password_again: '',
    email: '',
    chats: [],
  };

  componentDidMount() {
    if(this.props.auth.token){
      this.getUserChats(this.props.auth.token);
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.auth.token){
      this.getUserChats(nextProps.auth.token);
    }
  }

  getUserChats = async (token) => {
    axios.defaults.headers  = {
      'Content-Type': 'applocation/json',
      'Authorization': `Token ${token}`
    };
    try {
      const res = await axios.get('http://127.0.0.1:8000/chat/');
      console.log(res.data);
      this.setState({ chats: res.data });
    } catch(err) {
      alert(err.message);
    }
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmit = async e => {
    e.preventDefault();
    const { username, email, password, password_again } = this.state;
    if(this.state.login) this.props.authLogin(username, password);
    else this.props.authSignup(username, email, password, password_again);
  };

  render() {
    const activeChats = this.state.chats.map(chat => {
      return (
        <Contact key={chat.id} name={"Louis Potter"} chatUrl={`/${chat.id}`} status={"online"} />
      );
    });
    return (
      <div id="sidepanel">
        <div id="profile">
          <div className="wrap">
            <img id="profile-img" src="http://emilcarlsson.se/assets/mikeross.png" className="online" alt=""/>
            <p>Mike Ross</p>
            <i className="fa fa-chevron-down expand-button" aria-hidden="true"></i>
            <div id="status-options">
              <ul>
                <li id="status-online" className="active"><span className="status-circle"></span> <p>Online</p></li>
                <li id="status-away"><span className="status-circle"></span> <p>Away</p></li>
                <li id="status-busy"><span className="status-circle"></span> <p>Busy</p></li>
                <li id="status-offline"><span className="status-circle"></span> <p>Offline</p></li>
              </ul>
            </div>
            {/*<div id="expanded">*/}
              {/*<label htmlFor="twitter"><i className="fa fa-facebook fa-fw" aria-hidden="true"></i></label>*/}
              {/*<input name="twitter" type="text" value="mikeross"/>*/}
              {/*<label htmlFor="twitter"><i className="fa fa-twitter fa-fw" aria-hidden="true"></i></label>*/}
              {/*<input name="twitter" type="text" value="ross81"/>*/}
              {/*<label htmlFor="twitter"><i className="fa fa-instagram fa-fw" aria-hidden="true"></i></label>*/}
              {/*<input name="twitter" type="text" value="mike.ross"/>*/}
            {/*</div>*/}
          </div>
        </div>
        <div id="search">
          <label htmlFor=""><i className="fa fa-search" aria-hidden="true"></i></label>
          <input type="text" placeholder="Search contacts..."/>
        </div>
        <div id="contacts">
          <ul>
            {activeChats}
            {/*<Contact name={"Louis Potter"} chatUrl={"/louis"} status={"online"} />*/}
            {/*<Contact name={"Harvey Specter"} chatUrl={"/harvey"} status={"busy"} />*/}
          </ul>
          { this.props.auth.token ?
            <div>
              Hi, { this.props.auth.username } <br/>
              <Button onClick={e => { e.preventDefault(); this.props.logout(); }}>Logout</Button>
            </div>
            :
            <div>
            { this.state.login ?
              <Form>
                <Input label={"Username"} name={"username"} value={this.state.username} onChange={this.handleChange} floatingLabel />
                <Input label={"Password"} name={"password"} value={this.state.password} onChange={this.handleChange} floatingLabel />
                <Button onClick={this.handleSubmit}>Login</Button>
                Or
                <Button onClick={e => {e.preventDefault(); this.setState({ login: false })}}>Signup</Button>
              </Form>
              :
              <Form>
                <Input label={"Username"} name={"username"} value={this.state.username} onChange={this.handleChange} floatingLabel />
                <Input label={"Email"} name={"email"} value={this.state.email} onChange={this.handleChange} floatingLabel />
                <Input label={"Password"} name={"password"} value={this.state.password} onChange={this.handleChange} floatingLabel />
                <Input label={"Password Again"} name={"password_again"} value={this.state.password_again} onChange={this.handleChange} floatingLabel />
                <Button onClick={this.handleSubmit}>Signup</Button>
                Or
                <Button onClick={e => {e.preventDefault(); this.setState({ login: true })}}>Login</Button>
              </Form>
            }
          </div>
          }
        </div>
        <div id="bottom-bar">
          <button id="addcontact"><i className="fa fa-user-plus fa-fw" aria-hidden="true"></i>
            <span>Add contact</span></button>
          <button id="settings"><i className="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return { auth };
};

export default connect(mapStateToProps, { authLogin, authSignup, logout })(Sidepanel);