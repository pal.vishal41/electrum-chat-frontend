import React, { Component } from 'react';
import { connect } from "react-redux";
import './App.css';
import Chat from "./components/Chat";
import { checkAuth } from "./actions/auth";
import { BrowserRouter as Router, Route } from "react-router-dom";

class App extends Component {
  componentDidMount() {
    this.props.checkAuth();
  }

  render() {
    return (
      <Router>
        <React.Fragment>
          <Route path={"/:chatId"} component={Chat}/>
        </React.Fragment>
      </Router>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return { auth };
};

export default connect(mapStateToProps, { checkAuth })(App);
