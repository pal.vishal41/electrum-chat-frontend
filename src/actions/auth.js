import axios from 'axios';
import * as actionTypes from './actionTypes';

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  }
};

export const authSuccess = (token, username) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token,
    username,
  }
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error,
  }
};

export const logout = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('expirationDate');
  return {
    type: actionTypes.AUTH_LOGOUT
  }
};

const checkAuthTimeout = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout())
    }, expirationTime * 1000)
  }
};

export const authLogin = (username, password) => {
  return dispatch => {
    dispatch(authStart());
    axios.post('http://127.0.0.1:8000/rest-auth/login/', {
      username,
      password
    })
      .then(res => {
        const token = res.data.key;
        const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
        localStorage.setItem('token', token);
        localStorage.setItem('expirationDate', expirationDate.toString());
        dispatch(authSuccess(token, username));
        dispatch(checkAuthTimeout(3600));
      })
      .catch(error => {
        dispatch(authFail(error));
      })
  }
};

export const authSignup = (username, email, password1, password2) => {
  return dispatch => {
    dispatch(authStart());
    axios.post('http://127.0.0.1:8000/rest-auth/registration', {
      username,
      email,
      password1,
      password2
    })
      .then(res => {
        const token = res.data.key;
        const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
        localStorage.setItem('token', token);
        localStorage.setItem('expirationDate', expirationDate.toString());
        dispatch(authSuccess(token, username));
        dispatch(checkAuthTimeout(3600));
      })
      .catch(error => {
        dispatch(authFail(error));
      })
  }
};

export const checkAuth = () => {
  return async dispatch => {
    let token = await localStorage.getItem('token');
    if(!token) dispatch(logout());
    let expDate = await localStorage.getItem('expirationDate');
    if(new Date(expDate) < new Date()) dispatch(logout());
    else {
      let username = '';
      try {
        const headers = {
          'Authorization': `Token ${token}`,
        };
        const res = await axios.get('http://127.0.0.1:8000/rest-auth/user', {headers});
        username = res.data.username;
      } catch(err) {
        alert(err.message);
      }
      dispatch(authSuccess(token, username));
    }
  }
};